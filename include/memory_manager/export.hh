#pragma once

#ifdef MEMORY_MANAGER_WINDOWS
    #ifdef MEMORY_MANAGER_SHARED_LIBS
        #ifdef MEMORY_MANAGER_DLL_EXPORT
            #define MEMORY_MANAGER_API __declspec(dllexport)
        #else
            #define MEMORY_MANAGER_API __declspec(dllimport)
        #endif
    #else
        #define MEMORY_MANAGER_API
    #endif

#else
    #define MEMORY_MANAGER_API
#endif
