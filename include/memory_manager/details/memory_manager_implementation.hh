#pragma once
#include <list>
#include <vector>
#include <memory_manager/memory_manager.hh>

namespace memory_manager
{
    class memory_manager_implementation_type
    {
    public:
        using size_type = memory_manager_type::size_type;
        using byte_type = memory_manager_type::byte_type;
        using settings_type = memory_manager_type::settings_type;

        explicit memory_manager_implementation_type(const settings_type& settings);
        memory_manager_implementation_type(const memory_manager_implementation_type&) = delete;
        memory_manager_implementation_type(memory_manager_implementation_type&& other) noexcept;
        ~memory_manager_implementation_type() = default;
        memory_manager_implementation_type& operator=(const memory_manager_implementation_type&) = delete;
        memory_manager_implementation_type& operator=(memory_manager_implementation_type&& other) noexcept;

        void* allocate(size_type size);
        void deallocate(void* pointer);
    private:
        class block_type
        {
        public:
            explicit block_type(byte_type* begin);
            bool is_valid() const;
            byte_type* get_begin() const;

        private:
            byte_type* _begin;
        };

        class object_block_type: public block_type
        {
        public:
            explicit object_block_type(byte_type* begin);

            void set_size(size_type size) const;
            size_type get_size() const;

            void initialize_memory(const settings_type& settings) const;

            byte_type* get_object_pointer() const;

        private:
            static bool should_initialize(const settings_type& settings);
        };

        class free_block_base_type: public block_type
        {
        public:
            free_block_base_type(byte_type* begin, byte_type* next);

            size_type get_next() const;
            void set_next(size_type next) const;
            void set_next(const free_block_base_type& block) const;
            bool has_next() const;

        private:
            size_type* _next;
        };

        class free_block_type: public free_block_base_type
        {
        public:
            explicit free_block_type(byte_type* buffer);

            size_type get_size() const;
            void set_size(size_type size) const;

            byte_type* get_end() const;

            free_block_type get_next_block() const;

            void* allocate(size_type size,
                           const free_block_base_type& previous,
                           const settings_type& settings) const;
            void initialize_memory(const settings_type& settings) const;

            void merge(const free_block_type& next, const settings_type& settings) const;
        
        private:
            static size_type get_header_size();
            static bool should_initialize(const settings_type& settings);

            size_type* _size;
            byte_type* _end;
        };

        class start_block_type: public free_block_base_type
        {
        public:
            explicit start_block_type(byte_type* begin);

            free_block_type get_first_free_block() const;
        };

        class memory_block_type
        {
        public:
            memory_block_type(const settings_type& settings);

            start_block_type get_start_block() const;

            byte_type* data();

        private:
            using implementation_type = std::vector<byte_type>;

            implementation_type _implementation;
            start_block_type _start_block;
        };

        using memory_blocks_type = std::list<memory_block_type>;

        void* allocate(const memory_block_type& memory_block, size_type size) const;
        memory_blocks_type::iterator find_block(const byte_type* pointer);

        settings_type _settings;
        memory_blocks_type _memory_blocks;
    };
}
