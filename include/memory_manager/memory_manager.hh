#pragma once
#include <cstdint>
#include "export.hh"

namespace memory_manager
{
    class memory_manager_implementation_type;

    // A memory block will have the following layout
    // Types:
    // size_type: uint32_t
    // next_type: size_type
    // start_block_type: Contains a next_type pointer to the first free block in the memory block. The pointer is zero if the memory block is full
    // object_block_type: Contains a size_type, to store the size of the block, and the memory itself for the object
    // free_block_type: Contains a size_type, to store the size of the block, a next_type to point to the next free block in the memory block (if any)
    //                  and the memory for the block itself (The next pointer is zero, if there is no more free block in the memory block
    // used_memory_type: A memory block being used
    // free_memory_type: a memory block which is currently free
    //
    //E.g.
    // +------------------+------------------------------+------------------------------+------------------------------------------+------------------------------+------------------------------------------+
    // | start_block_type | object_block_type            | object_block_type            | free_block_type                          | object_block_type            | free_block_type                          |
    // +------------------+-----------+------------------+-----------+------------------+-----------+-----------+------------------+-----------+------------------+-----------+-----------+------------------+
    // | next_type        | size_type | used_memory_type | size_type | used_memory_type | size_type | next_type | free_memory_type | size_type | used_memory_type | size_type | next_type | free_memory_type | etc.
    // +------------------+-----------+------------------+-----------+------------------+-----------+-----------+------------------+-----------+------------------+-----------+-----------+------------------+
    //       |                                                                                ^           |                                                             ^
    //       +--------------------------------------------------------------------------------+           +-------------------------------------------------------------+
    class MEMORY_MANAGER_API memory_manager_type
    {
    public:
        using byte_type = uint8_t;
        using size_type = uint32_t;

        class MEMORY_MANAGER_API settings_type
        {
        public:
            enum class block_initialization_type
            {
                // Do not initialize blocks
                none,
                // Initialize free blocks
                free,
                // Initialize object blocks
                object,
                // Initialize both free and object blocks
                both,
            };

            explicit settings_type(size_type memory_block_size);

            settings_type& set_memory_block_size(size_type memory_block_size);
            settings_type& set_minimum_free_block_size(size_type minimum_free_block_size);
            settings_type& set_block_initialization(block_initialization_type block_initialization);
            settings_type& set_free_block_initialize_value(byte_type initialize_value);
            settings_type& set_object_block_initialize_value(byte_type initialize_value);

            size_type get_memory_block_size() const;
            size_type get_minimum_free_block_size() const;
            block_initialization_type get_block_initialization() const;
            byte_type get_free_block_initialize_value() const;
            byte_type get_object_block_initialize_value() const;

        private:
            size_type _memory_block_size;
            size_type _minimum_free_block_size = sizeof(size_type);
            block_initialization_type _block_initialization = block_initialization_type::none;
            byte_type _free_block_initialize_value = 0;
            byte_type _object_block_initialize_value = 0;
        };

        explicit memory_manager_type(const settings_type& settings);
        memory_manager_type(const memory_manager_type&) = delete;
        memory_manager_type(memory_manager_type&& other) noexcept;
        ~memory_manager_type();
        memory_manager_type& operator=(const memory_manager_type&) = delete;
        memory_manager_type& operator=(memory_manager_type&& other) noexcept;
        
        void* allocate(size_type size);
        void deallocate(void* pointer);

    private:
        memory_manager_implementation_type* _implementation = nullptr;
    };
}
