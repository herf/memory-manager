#include <memory_manager/details/memory_manager_implementation.hh>
#include <cstring>

namespace memory_manager
{
    //
    // memory_manager_implementation_type
    //

    memory_manager_implementation_type::memory_manager_implementation_type(const settings_type& settings):
        _settings(settings)
    {
        _memory_blocks.emplace_back(_settings);
    }

    memory_manager_implementation_type::memory_manager_implementation_type(memory_manager_implementation_type&& other) noexcept:
        _settings(other._settings),
        _memory_blocks(std::move(other._memory_blocks))
    {
        // Nothing to do yet
    }

    memory_manager_implementation_type& memory_manager_implementation_type::operator=(memory_manager_implementation_type&& other) noexcept
    {
        if(&other != this)
        {
            _settings = other._settings;
            _memory_blocks = std::move(other._memory_blocks);
        }

        return *this;
    }

    void* memory_manager_implementation_type::allocate(size_type size)
    {
        // The biggest request we can handle is the size of the memory block,
        // minus the start block (4 bytes) and the object's block size (4 bytes)
        if (size > _settings.get_memory_block_size() - (2 * sizeof(size_type)))
        {
            return nullptr;
        }
        for (memory_block_type &memory_block : _memory_blocks)
        {
            void* const result = allocate(memory_block, size);

            if(result != nullptr)
            {
                return result;
            }
        }
        // Create a new memory block, and allocate the needed size there
        _memory_blocks.emplace_back(_settings);
        return allocate(_memory_blocks.back(), size);
    }

    void memory_manager_implementation_type::deallocate(void* pointer)
    {
        byte_type* data = reinterpret_cast<byte_type*>(pointer);
        const memory_blocks_type::iterator iterator = find_block(data);

        if(iterator != _memory_blocks.end())
        {
            const start_block_type start_block = iterator->get_start_block();
            const free_block_type current(data - sizeof(size_type));

            current.initialize_memory(_settings);
            if(start_block.has_next())
            {
                free_block_type next = start_block.get_first_free_block();

                if(next.get_begin() > data)
                {
                    start_block.set_next(current);
                    current.set_next(next);
                    current.merge(next, _settings);
                }
                else
                {
                    while(next.has_next())
                    {
                        const free_block_type previous = next;

                        next = next.get_next_block();
                        if(next.get_begin() > data)
                        {
                            previous.set_next(current);
                            current.set_next(next);
                            current.merge(next, _settings);
                            previous.merge(current, _settings);

                            break;
                        }
                    }
                    if(next.get_begin() < data)
                    {
                        // There is no free block after the current one,
                        // so in this case, 'next' is the 'previous'
                        next.set_next(current);
                        current.set_next(0);
                        next.merge(current, _settings);
                    }
                }
            }
            else
            {
                // There is no free block in this memory block
                start_block.set_next(current);
                current.set_next(0);
            }
        }
        else
        {
            // This pointer is not managed by us
            // Cannot do much about it, we should not raise any exception here
        }
    }

    void* memory_manager_implementation_type::allocate(const memory_block_type& memory_block, size_type size) const
    {
        const start_block_type start_block = memory_block.get_start_block();
        void* result = nullptr;

        if(start_block.has_next())
        {
            free_block_type next_block = start_block.get_first_free_block();

            result = next_block.allocate(size,
                                         start_block,
                                         _settings);
            while(result == nullptr && next_block.has_next())
            {
                free_block_type previous_block = next_block;

                next_block = next_block.get_next_block();
                result = next_block.allocate(size,
                                             previous_block,
                                             _settings);
            }
        }

        return result;
    }

    memory_manager_implementation_type::memory_blocks_type::iterator memory_manager_implementation_type::find_block(const byte_type* pointer)
    {
        for(memory_blocks_type::iterator iterator = _memory_blocks.begin(); iterator != _memory_blocks.end(); ++iterator)
        {
            const byte_type* const data = iterator->data();

            if(pointer >= data && pointer < data + _settings.get_memory_block_size())
            {
                return iterator;
            }
        }
        
        return _memory_blocks.end();
    }

    //
    // memory_manager_implementation_type::block_type
    //

    memory_manager_implementation_type::block_type::block_type(byte_type* begin): _begin(begin)
    {
        // Nothing to do yet
    }

    bool memory_manager_implementation_type::block_type::is_valid() const
    {
        return _begin != nullptr;
    }

    memory_manager_implementation_type::byte_type* memory_manager_implementation_type::block_type::get_begin() const
    {
        return _begin;
    }

    //
    // memory_manager_implementation_type::object_block_type
    //

    memory_manager_implementation_type::object_block_type::object_block_type(byte_type* begin): block_type(begin)
    {
        // Nothing to do yet
    }

    void memory_manager_implementation_type::object_block_type::set_size(size_type size) const
    {
        *reinterpret_cast<size_type*>(get_begin()) = size;
    }

    memory_manager_implementation_type::size_type memory_manager_implementation_type::object_block_type::get_size() const
    {
        return *reinterpret_cast<size_type*>(get_begin());
    }

    void memory_manager_implementation_type::object_block_type::initialize_memory(const settings_type& settings) const
    {
        if(should_initialize(settings))
            memset(get_begin() + sizeof(size_type), settings.get_object_block_initialize_value(), get_size());
    }

    memory_manager_implementation_type::byte_type* memory_manager_implementation_type::object_block_type::get_object_pointer() const
    {
        return get_begin() + sizeof(size_type);
    }

    bool memory_manager_implementation_type::object_block_type::should_initialize(const settings_type& settings)
    {
        return settings.get_block_initialization() == settings_type::block_initialization_type::object ||
               settings.get_block_initialization() == settings_type::block_initialization_type::both;
    }

    //
    // memory_manager_implementation_type::free_block_base_type
    //

    memory_manager_implementation_type::free_block_base_type::free_block_base_type(byte_type* begin, byte_type* next):
        block_type(begin),
        _next(reinterpret_cast<size_type*>(next))
    {
        // Nothing to do yet
    }

    memory_manager_implementation_type::size_type memory_manager_implementation_type::free_block_base_type::get_next() const
    {
        return *_next;
    }

    void memory_manager_implementation_type::free_block_base_type::set_next(size_type next) const
    {
        *_next = next;
    }

    void memory_manager_implementation_type::free_block_base_type::set_next(const free_block_base_type& block) const
    {
        if(block.is_valid())
        {
            set_next(static_cast<size_type>(block.get_begin() - reinterpret_cast<byte_type*>(_next)));
        }
        else
        {
            set_next(0);
        }
    }

    bool memory_manager_implementation_type::free_block_base_type::has_next() const
    {
        return get_next() != 0;
    }

    //
    // memory_manager_implementation_type::free_block_type
    //

    memory_manager_implementation_type::free_block_type::free_block_type(byte_type* buffer):
        free_block_base_type(buffer, buffer == nullptr ? nullptr : buffer + sizeof(size_type)),
        _size(reinterpret_cast<size_type*>(buffer)),
        _end(buffer == nullptr ? nullptr : buffer + *_size + sizeof(size_type))
    {
        // Nothing to do yet
    }

    memory_manager_implementation_type::size_type memory_manager_implementation_type::free_block_type::get_size() const
    {
        return *_size;
    }

    void memory_manager_implementation_type::free_block_type::set_size(size_type size) const
    {
        *_size = size;
    }

    memory_manager_implementation_type::byte_type* memory_manager_implementation_type::free_block_type::get_end() const
    {
        return _end;
    }

    memory_manager_implementation_type::free_block_type memory_manager_implementation_type::free_block_type::get_next_block() const
    {
        if(!has_next())
            return free_block_type(nullptr);
        return free_block_type(get_begin() + get_next() + sizeof(size_type));
    }

    void* memory_manager_implementation_type::free_block_type::allocate(size_type size,
                                                                        const free_block_base_type& previous,
                                                                        const settings_type& settings) const
    {
        if(get_size() < size)
        {
            return nullptr;
        }

        // The actual change in size of the free block - since we need space to store the block size
        const size_type size_change = size + sizeof(size_type);
        const size_type remaining_size =  size_change > get_size() ? 0 : get_size() - size_change;

        // Check if it is worth creating a new free block,
        // or we should just allocate the whole block
        if(remaining_size >= settings.get_minimum_free_block_size())
        {
            // Create the object block
            const object_block_type object_block(get_end() - size_change);

            object_block.set_size(size);
            object_block.initialize_memory(settings);

            // Update our size
            set_size(remaining_size);

            return object_block.get_object_pointer();
        }

        // Convert the whole free block into an object block
        const object_block_type object_block(get_begin());

        // Update previous free block
        previous.set_next(get_next_block());

        // No need to set size, since it did not change
        object_block.initialize_memory(settings);

        return object_block.get_object_pointer();
    }

    void memory_manager_implementation_type::free_block_type::initialize_memory(const settings_type& settings) const
    {
        if(should_initialize(settings))
            memset(get_begin() + get_header_size(), settings.get_free_block_initialize_value(), get_size() - sizeof(size_type));
    }

    void memory_manager_implementation_type::free_block_type::merge(const free_block_type& next, const settings_type& settings) const
    {
        if(get_begin() + sizeof(size_type) + get_size() == next.get_begin())
        {
            set_size(get_size() + next.get_size() + sizeof(size_type));
            set_next(next.get_next_block());
            if(should_initialize(settings))
            {
                memset(next.get_begin(), settings.get_free_block_initialize_value(), get_header_size());
            }
        }
    }

    memory_manager_implementation_type::size_type memory_manager_implementation_type::free_block_type::get_header_size()
    {
        return sizeof(size_type) + sizeof(size_type);
    }

    bool memory_manager_implementation_type::free_block_type::should_initialize(const settings_type& settings)
    {
        return settings.get_block_initialization() == settings_type::block_initialization_type::free ||
               settings.get_block_initialization() == settings_type::block_initialization_type::both;
    }

    //
    // memory_manager_implementation_type::start_block_type
    //

    memory_manager_implementation_type::start_block_type::start_block_type(byte_type* begin):
        free_block_base_type(begin, begin)
    {
        // Nothing to do yet
    }

    memory_manager_implementation_type::free_block_type memory_manager_implementation_type::start_block_type::get_first_free_block() const
    {
        if(!has_next())
        {
            return free_block_type(nullptr);
        }

        return free_block_type(get_begin() + get_next());
    }

    //
    // memory_manager_implementation_type::memory_block_type
    //

    memory_manager_implementation_type::memory_block_type::memory_block_type(const settings_type& settings):
        _implementation(settings.get_memory_block_size()), _start_block(_implementation.data())
    {
        _start_block.set_next(sizeof(size_type));
        const free_block_type free_block = _start_block.get_first_free_block();

        free_block.set_size(settings.get_memory_block_size() - 2 * sizeof(size_type));
        free_block.set_next(0);
        free_block.initialize_memory(settings);
    }

    memory_manager_implementation_type::start_block_type memory_manager_implementation_type::memory_block_type::get_start_block() const
    {
        return _start_block;
    }

    memory_manager_implementation_type::byte_type* memory_manager_implementation_type::memory_block_type::data()
    {
        return _implementation.data();
    }
}
