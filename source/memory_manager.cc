#include <memory_manager/memory_manager.hh>
#include <algorithm>
#include <memory_manager/details/memory_manager_implementation.hh>

namespace memory_manager
{
    //
    // memory_manager_type::settings_type
    //
    //

    memory_manager_type::settings_type::settings_type(size_type memory_block_size): _memory_block_size(memory_block_size)
    {
        // Nothing to do yet
    }

    memory_manager_type::settings_type& memory_manager_type::settings_type::set_memory_block_size(size_type memory_block_size)
    {
        _memory_block_size = memory_block_size;

        return *this;
    }

    memory_manager_type::settings_type& memory_manager_type::settings_type::set_minimum_free_block_size(size_type minimum_free_block_size)
    {
        // The minimum size of a free_block type, has to be at least the size of 'size_type', so we can write the next pointer into it
        _minimum_free_block_size = std::max(minimum_free_block_size, size_type(sizeof(size_type)));

        return *this;
    }

    memory_manager_type::settings_type& memory_manager_type::settings_type::set_block_initialization(block_initialization_type block_initialization)
    {
        _block_initialization = block_initialization;

        return *this;
    }

    memory_manager_type::settings_type& memory_manager_type::settings_type::set_free_block_initialize_value(byte_type initialize_value)
    {
        _free_block_initialize_value = initialize_value;

        return *this;
    }

    memory_manager_type::settings_type& memory_manager_type::settings_type::set_object_block_initialize_value(byte_type initialize_value)
    {
        _object_block_initialize_value = initialize_value;

        return *this;
    }

    memory_manager_type::size_type memory_manager_type::settings_type::get_memory_block_size() const
    {
        return _memory_block_size;
    }

    memory_manager_type::size_type memory_manager_type::settings_type::get_minimum_free_block_size() const
    {
        return _minimum_free_block_size;
    }

    memory_manager_type::settings_type::block_initialization_type memory_manager_type::settings_type::get_block_initialization() const
    {
        return _block_initialization;
    }

    memory_manager_type::byte_type memory_manager_type::settings_type::get_free_block_initialize_value() const
    {
        return _free_block_initialize_value;
    }

    memory_manager_type::byte_type memory_manager_type::settings_type::get_object_block_initialize_value() const
    {
        return _object_block_initialize_value;
    }

    //
    // memory_manager_type
    //

    memory_manager_type::memory_manager_type(const settings_type& settings):
        _implementation(new memory_manager_implementation_type(settings))
    {
        // Nothing to do yet
    }

    memory_manager_type::memory_manager_type(memory_manager_type&& other) noexcept:
        _implementation(other._implementation)
    {
        other._implementation = nullptr;
    }

    memory_manager_type::~memory_manager_type()
    {
        if(nullptr != _implementation)
        {
            delete _implementation;
            _implementation = nullptr;
        }
    }

    memory_manager_type& memory_manager_type::operator=(memory_manager_type&& other) noexcept
    {
        if(&other != this)
        {
            _implementation = other._implementation;
            other._implementation = nullptr;
        }

        return *this;
    }

    void* memory_manager_type::allocate(size_type size)
    {
        return _implementation->allocate(size);
    }

    void memory_manager_type::deallocate(void* pointer)
    {
        _implementation->deallocate(pointer);
    }
}


