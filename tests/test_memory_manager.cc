#include <catch2/catch.hpp>
#include <memory_manager/memory_manager.hh>

using namespace memory_manager;

namespace
{
    class memory_reader_type
    {
    public:
        explicit memory_reader_type(memory_manager_type::byte_type* buffer): _buffer(buffer)
        {
            // Nothing to do yet
        }

        template<typename value_type>
        value_type read(memory_manager_type::size_type offset = 0)
        {
            return *reinterpret_cast<value_type*>(_buffer + offset);
        }
    
    private:
        memory_manager_type::byte_type* _buffer;
    };

    class free_block_type
    {
    public:
        explicit free_block_type(memory_manager_type::byte_type* begin): _begin(begin)
        {
            // Nothing to do yet
        }

        memory_manager_type::size_type get_size() const
        {
            return *reinterpret_cast<memory_manager_type::size_type*>(_begin);
        }

        memory_manager_type::size_type get_next() const
        {
            const memory_manager_type::size_type offset = sizeof(memory_manager_type::size_type);

            return *reinterpret_cast<memory_manager_type::size_type*>(_begin + offset);
        }

        bool check_block(memory_manager_type::byte_type expected_value) const
        {
            memory_manager_type::byte_type* begin = _begin + 2 * sizeof(memory_manager_type::size_type);
            memory_manager_type::byte_type* end = begin + get_size() - sizeof(memory_manager_type::size_type);

            while(begin != end)
            {
                if(*begin != expected_value)
                {
                    return false;
                }
                ++begin;
            }

            return true;
        }

    private:
        memory_manager_type::byte_type* _begin;
    };

    class object_block_type
    {
    public:
        explicit object_block_type(memory_manager_type::byte_type* begin): _begin(begin)
        {
            // Nothing to do yet
        }

        memory_manager_type::size_type get_size() const
        {
            return *reinterpret_cast<memory_manager_type::size_type*>(_begin);
        }

        bool check_block(memory_manager_type::byte_type expected_value) const
        {
            memory_manager_type::byte_type* begin = _begin + sizeof(memory_manager_type::size_type);
            memory_manager_type::byte_type* end = begin + get_size();

            while(begin != end)
            {
                if(*begin != expected_value)
                {
                    return false;
                }
                ++begin;
            }

            return true;
        }

    private:
        memory_manager_type::byte_type* _begin;
    };
}

SCENARIO("memory_manager_type objects can be used to allocate/deallocate objects")
{
    GIVEN("A memory_manager_type object with no allocations yet")
    {
        const memory_manager_type::size_type block_size = 64;
        const memory_manager_type::settings_type::block_initialization_type block_initialization =
            memory_manager_type::settings_type::block_initialization_type::both;
        const memory_manager_type::byte_type free_block_initialize_value = 254;
        const memory_manager_type::byte_type object_block_initialize_value = 255;       
        memory_manager_type memory_manager(memory_manager_type::settings_type(block_size).set_block_initialization(block_initialization)
                                                                                         .set_free_block_initialize_value(free_block_initialize_value)
                                                                                         .set_object_block_initialize_value(object_block_initialize_value)
                                                                                         .set_memory_block_size(block_size));

        THEN("This object can be used to allocate memory")
        {
            using size_type = memory_manager_type::size_type;
            using byte_type = memory_manager_type::byte_type;

            const memory_manager_type::size_type object_size_a = 32;
            void* pointer_a = memory_manager.allocate(object_size_a);
            // The new object should allocated at the end of the memory block, so it should be 32 bytes from the beginning
            byte_type* memory_block_a = reinterpret_cast<byte_type*>(pointer_a) - 32;
            memory_reader_type memory_reader_a(memory_block_a);

            CHECK(pointer_a != nullptr);
            // The start block should point to the first free block, which should be at the beginning of the memory block
            CHECK(memory_reader_a.read<size_type>() == 4);

            // Where the start block points, there should be a free block, with size 20
            // and no other free blocks after that
            const free_block_type free_block_a(memory_block_a + 4);

            CHECK(free_block_a.get_size() == 20);
            CHECK(free_block_a.get_next() == 0);
            CHECK(free_block_a.check_block(free_block_initialize_value));

            // The object block should be after the free block
            const object_block_type object_block_a(memory_block_a + 28);

            CHECK(object_block_a.get_size() == object_size_a);
            CHECK(object_block_a.check_block(object_block_initialize_value));

            AND_THEN("The object can be used to allocate additional objects")
            {
                const memory_manager_type::size_type object_size_b = 8;
                void* pointer_b = memory_manager.allocate(object_size_b);

                CHECK(pointer_b != nullptr);
                // The start block should still point to the first free block, which should still be at the beginning of the memory block
                CHECK(memory_reader_a.read<size_type>() == 4);
                // Where the start block points, there should be a free block, with only size 8 left
                // and no other free blocks after that
                const free_block_type free_block_b(memory_block_a + 4);

                CHECK(free_block_b.get_size() == 8);
                CHECK(free_block_b.get_next() == 0);
                CHECK(free_block_b.check_block(free_block_initialize_value));

                // A new object block should have been created for the new object with its size
                const object_block_type object_block_b(memory_block_a + 16);

                CHECK(object_block_b.get_size() == object_size_b);
                CHECK(object_block_b.check_block(object_block_initialize_value));

                // The object block for the first object should still be there
                CHECK(object_block_a.get_size() == object_size_a);
                CHECK(object_block_a.check_block(object_block_initialize_value));

                // The two objects should be laid out sequentially, and the distance between then could be determined
                // The second object should be "before" the first one by 12 bytes (its own size + 4 bytes for storing the block's size
                CHECK(reinterpret_cast<byte_type*>(pointer_a) - reinterpret_cast<byte_type*>(pointer_b) == 12);

                const memory_manager_type::size_type object_size_c = 8;
                void* pointer_c = memory_manager.allocate(object_size_c);

                CHECK(pointer_c != nullptr);
                // There should be no more free block in this memory block, so the start block should not point to anywhere
                CHECK(memory_reader_a.read<size_type>() == 0);

                // The new object should have been allocated at the beginning of the memory block
                const object_block_type object_block_c(memory_block_a + 4);

                CHECK(object_block_c.get_size() == object_size_c);
                CHECK(object_block_c.check_block(object_block_initialize_value));

                // The second object block should still be there
                CHECK(object_block_b.get_size() == object_size_b);
                CHECK(object_block_b.check_block(object_block_initialize_value));
                // The first object block also should still be there
                CHECK(object_block_a.get_size() == object_size_a);
                CHECK(object_block_a.check_block(object_block_initialize_value));
                
                // The distance between the 2nd and the 3rd memory block should be equal to 8 bytes (for the object itself)
                // plus 4 bytes (for storing the size of the object block)
                CHECK(reinterpret_cast<byte_type*>(pointer_b) - reinterpret_cast<byte_type*>(pointer_c) == 12);

                // Next: Allocate yet another block, so the manager needs to create a whole new memory block for that
                memory_manager_type::size_type object_size_d = 16;
                void* pointer_d = memory_manager.allocate(object_size_d);
                byte_type* memory_block_b = reinterpret_cast<byte_type*>(pointer_d) - 48;
                memory_reader_type memory_reader_b(memory_block_b);

                CHECK(pointer_d != nullptr);
                // Check if a new memory block has been created
                CHECK(memory_block_a != memory_block_b);
                // The start block should point to the first free block at the beginning of the memory block
                CHECK(memory_reader_b.read<size_type>() == 4);

                // The only free block in the memory block should cover the whole remaining memory
                const free_block_type free_block_d(memory_block_b + 4);

                CHECK(free_block_d.get_size() == 36);
                CHECK(free_block_d.get_next() == 0);
                CHECK(free_block_d.check_block(free_block_initialize_value));

                // A new object block should have been created for the new allocation
                const object_block_type object_block_d(memory_block_b + 44);

                CHECK(object_block_d.get_size() == object_size_d);
                CHECK(object_block_d.check_block(object_block_initialize_value));

                AND_THEN("The allocated objects can be deallocated and there memory will be added back as free memory")
                {
                    // Next: test deleting pointer_a, and allocate a new one with less size, like 16 bytes
                    memory_manager.deallocate(pointer_a);
                    // The start block now should point to the newly created free block, where the first object used to be
                    CHECK(memory_reader_a.read<size_type>() == 28);
                    // At the place of pointer_a, now there should be a free memory block, followed by nothing
                    const free_block_type freed_block_a(memory_block_a + 28);

                    CHECK(freed_block_a.get_size() == object_size_a);
                    CHECK(freed_block_a.get_next() == 0);
                    CHECK(freed_block_a.check_block(free_block_initialize_value));

                    memory_manager.deallocate(pointer_c);
                    // Now the start block should point to the free block, which was created at the third object's place
                    // since now that free block is the first one
                    CHECK(memory_reader_a.read<size_type>() == 4);
                    // At the place of pointer_c, now there should be a free memory block,
                    // followed by the free block at the firs object's place
                    const free_block_type freed_block_c(memory_block_a + 4);

                    CHECK(freed_block_c.get_size() == object_size_c);
                    CHECK(freed_block_c.get_next() == 20);
                    CHECK(freed_block_c.check_block(free_block_initialize_value));
                    // The free block at the first object's place should still be there untouched
                    CHECK(freed_block_a.get_size() == object_size_a);
                    CHECK(freed_block_a.get_next() == 0);
                    CHECK(freed_block_a.check_block(free_block_initialize_value));

                    memory_manager.deallocate(pointer_b);
                    // Now the whole block should be free again, so the start block should point to the very first free block
                    CHECK(memory_reader_a.read<size_type>() == 4);
                    // And that free block should cover the whole memory block
                    const free_block_type freed_block_b(memory_block_a + 4);

                    CHECK(freed_block_b.get_size() == 56);
                    CHECK(freed_block_b.get_next() == 0);
                    CHECK(freed_block_b.check_block(free_block_initialize_value));
                }
            }
        }
    }
}

SCENARIO("memory_manager_type objects can be configured to have a minimum size of free blocks")
{
    GIVEN("A memory_manager_type object with no allocations yet and a non-default minimum free block size")
    {
        memory_manager_type memory_manager(memory_manager_type::settings_type(64).set_minimum_free_block_size(32));

        THEN("This object can be used to allocate memory")
        {
            void* pointer_a = memory_manager.allocate(16);
            memory_manager_type::byte_type* memory_block_a = static_cast<memory_manager_type::byte_type*>(pointer_a) - 48;
            memory_reader_type memory_reader(memory_block_a);

            CHECK(pointer_a != nullptr);
            // The start block should point to the only free block at the beginning
            CHECK(memory_reader.read<memory_manager_type::size_type>() == 4);
            const free_block_type free_block_a(memory_block_a + 4);
            CHECK(free_block_a.get_size() == 36);
            CHECK(free_block_a.get_next() == 0);

            AND_THEN("The setting for the minimum free block size will take effect")
            {
                void* pointer_b = memory_manager.allocate(16);

                CHECK(pointer_b != nullptr);
                // There should be no free block in the memory block,
                // since the minimum free block size, could not be satisfied
                // within this block
                CHECK(memory_reader.read<memory_manager_type::size_type>() == 0);

                AND_THEN("The next allocation should be done in a new memory block")
                {
                    void* pointer_c = memory_manager.allocate(16);
                    memory_manager_type::byte_type* memory_block_b = static_cast<memory_manager_type::byte_type*>(pointer_c) - 48;

                    CHECK(memory_block_a != memory_block_b);
                }
            }
        }
    }
}

SCENARIO("memory_manager_type objects return nullptr in case of errors")
{
    GIVEN("A memory_manager_type object with no allocations yet")
    {
        memory_manager_type memory_manager(memory_manager_type::settings_type(64));
        void* pointer_a = memory_manager.allocate(56);
        void* pointer_b = memory_manager.allocate(60);

        // This memory manager cannot handle requests for more than 56 bytes
        CHECK(pointer_a != nullptr);
        // This memory manager cannot handle requests for more than 56 bytes
        CHECK(pointer_b == nullptr);
    }
}

SCENARIO("memory_manager_type object can be used with real types too")
{
    GIVEN("A type which allocates memory using a memory_manager_type object")
    {
        class allocator_type
        {
        public:
            static allocator_type& get_instance()
            {
                static allocator_type instance;

                return instance;
            }

            void* allocate(memory_manager_type::size_type size)
            {
                return _memory_manager.allocate(size);
            }

            void deallocate(void* pointer)
            {
                _memory_manager.deallocate(pointer);
            }

        private:
            allocator_type():
                _memory_manager(memory_manager_type::settings_type(64).set_block_initialization(memory_manager_type::settings_type::block_initialization_type::both)
                                                                      .set_free_block_initialize_value(254)
                                                                      .set_object_block_initialize_value(255))
            {
                // Nothing to do yet
            }

            memory_manager_type _memory_manager;
        };

        AND_GIVEN("A type capable of using the memory manager")
        {
            class test_type
            {
            public:
                test_type(int i, float f, double d): _i(i), _f(f), _d(d)
                {
                    // Nothing to go yet
                }

                void* operator new(size_t size)
                {
                    return allocator_type::get_instance().allocate(static_cast<memory_manager_type::size_type>(size));
                }

                void operator delete(void* pointer)
                {
                    allocator_type::get_instance().deallocate(pointer);
                }

                int get_i() const
                {
                    return _i;
                }

                float get_f() const
                {
                    return _f;
                }

                double get_d() const
                {
                    return _d;
                }

            private:
                int _i;
                float _f;
                double _d;
            };

            THEN("Objects of this type can be created easily")
            {
                test_type* test_a = new test_type(1, 2.0f, 3.0);
                test_type* test_b = new test_type(10, 20.0f, 30.0);

                REQUIRE(test_a != nullptr);
                CHECK(test_a->get_i() == 1);
                CHECK(test_a->get_f() == 2.0f);
                CHECK(test_a->get_d() == 3.0);

                REQUIRE(test_b != nullptr);
                CHECK(test_b->get_i() == 10);
                CHECK(test_b->get_f() == 20.0f);
                CHECK(test_b->get_d() == 30.0);
                // TODO: Test the memory block in the memory manager too?

                AND_THEN("The created objects can be deleted easily")
                {
                    delete test_a;
                    delete test_b;

                    // After deleting the object, we can still safely read from it,
                    // since the memory is still kept within the application,
                    // but the data should not be there
                    CHECK(test_a->get_i() != 1);
                    // The memory where the object lied should be marked for free, with 254 written to every byte
                    CHECK(test_a->get_i() == (254u << 24) + (254u << 16) + (254u << 8) + 254u);
                    CHECK(test_a->get_f() != 2.0f);
                    CHECK(test_a->get_d() != 3.0);

                    CHECK(test_b->get_i() != 10);
                    // The memory where the object lied should be marked for free, with 254 written to every byte
                    CHECK(test_b->get_i() == (254u << 24) + (254u << 16) + (254u << 8) + 254u);
                    CHECK(test_b->get_f() != 20.0f);
                    CHECK(test_b->get_d() != 30.0);
                }
            }
        }
        AND_GIVEN("A type hierarchy capable of using the memory manager")
        {
            class parent_type
            {
            public:
                parent_type(int i): _i(i)
                {
                    // Nothing to do yet
                }

                virtual ~parent_type() = default;

                virtual int execute(int a, int b) = 0;

                void* operator new(size_t size)
                {
                    return allocator_type::get_instance().allocate(static_cast<memory_manager_type::size_type>(size));
                }

                void operator delete(void* pointer)
                {
                    allocator_type::get_instance().deallocate(pointer);
                }

                int get_i() const
                {
                    return _i;
                }

            private:
                int _i;
            };

            class add_type: public parent_type
            {
            public:
                add_type(int i, float f): parent_type(i), _f(f)
                {
                    // Nothing to do yet
                }

                virtual int execute(int a, int b) override
                {
                    return a + b;
                }

                float get_f() const
                {
                    return _f;
                }

            private:
                float _f;
            };

            class multiply_type: public parent_type
            {
            public:
                multiply_type(int i, double d): parent_type(i), _d(d)
                {
                    // Nothing to do yet
                }

                virtual int execute(int a, int b) override
                {
                    return a * b;
                }

                double get_d() const
                {
                    return _d;
                }

            private:
                double _d;
            };

            THEN("Objects of this type can be created easily")
            {
                parent_type* add = new add_type(5, 10.0f);
                parent_type* multiply = new multiply_type(50, 100.0);

                REQUIRE(add != nullptr);
                CHECK(add->execute(3, 5) == 8);
                CHECK(add->get_i() == 5);
                REQUIRE(dynamic_cast<add_type*>(add) != nullptr);
                CHECK(dynamic_cast<add_type*>(add)->get_f() == 10.0f);

                REQUIRE(multiply != nullptr);
                CHECK(multiply->execute(3, 5) == 15);
                CHECK(multiply->get_i() == 50);
                REQUIRE(dynamic_cast<multiply_type*>(multiply) != nullptr);
                CHECK(dynamic_cast<multiply_type*>(multiply)->get_d() == 100.0);
                // TODO: Test the memory block in the memory manager too?

                AND_THEN("The created objects can be deleted easily")
                {
                    delete add;
                    delete multiply;

                    // After deleting the object, we can still safely read from it,
                    // since the memory is still kept within the application,
                    // but the data should not be there
                    CHECK(add->get_i() != 5);
                    // The memory where the object lied should be marked for free, with 254 written to every byte
                    CHECK(add->get_i() == (254u << 24) + (254u << 16) + (254u << 8) + 254u);

                    CHECK(multiply->get_i() != 50);
                    // The memory where the object lied should be marked for free, with 254 written to every byte
                    CHECK(multiply->get_i() == (254u << 24) + (254u << 16) + (254u << 8) + 254u);
                }
            }
        }
    }
}

SCENARIO("memory_manager_type objects can be moved but not copied, and they will continue to work after being moved")
{
    GIVEN("A memory_manager_type object with no allocations yet")
    {
        using byte_type = memory_manager_type::byte_type;
        using size_type = memory_manager_type::size_type;

        const size_type free_block_initialize_value = 255;
        const size_type object_block_initialize_value = 254;
        memory_manager_type memory_manager_a(memory_manager_type::settings_type(64).set_block_initialization(memory_manager_type::settings_type::block_initialization_type::both)
                                                                                   .set_free_block_initialize_value(free_block_initialize_value)
                                                                                   .set_object_block_initialize_value(object_block_initialize_value));

        THEN("This object can be used to do allocations")
        {
            const size_type object_size_a = 32;
            const size_type object_size_b = 8;
            void* pointer_a = memory_manager_a.allocate(object_size_a);

            // The new object should allocated at the end of the memory block, so it should be 32 bytes from the beginning
            byte_type* memory_block_a = reinterpret_cast<byte_type*>(pointer_a) - 32;
            memory_reader_type memory_reader_a(memory_block_a);

            CHECK(pointer_a != nullptr);
            // The start block should point to the first free block, which should be at the beginning of the memory block
            CHECK(memory_reader_a.read<size_type>() == 4);

            // Where the start block points, there should be a free block, with size 20
            // and no other free blocks after that
            const free_block_type free_block_a(memory_block_a + 4);

            CHECK(free_block_a.get_size() == 20);
            CHECK(free_block_a.get_next() == 0);
            CHECK(free_block_a.check_block(free_block_initialize_value));
            // The object block should be after the free block
            const object_block_type object_block_a(memory_block_a + 28);

            CHECK(object_block_a.get_size() == object_size_a);
            CHECK(object_block_a.check_block(object_block_initialize_value));
            AND_THEN("This memory_manager_type object can be moved into an other (new) object and the new object can be used")
            {
                memory_manager_type memory_manager_b(std::move(memory_manager_a));
                void* pointer_b = memory_manager_b.allocate(object_size_b);

                CHECK(pointer_b != nullptr);
                // The start block should still point to the first free block, which should still be at the beginning of the memory block
                CHECK(memory_reader_a.read<size_type>() == 4);
                // Where the start block points, there should be a free block, with only size 8 left
                // and no other free blocks after that
                const free_block_type free_block_b(memory_block_a + 4);

                CHECK(free_block_b.get_size() == 8);
                CHECK(free_block_b.get_next() == 0);
                CHECK(free_block_b.check_block(free_block_initialize_value));

                // A new object block should have been created for the new object with its size
                const object_block_type object_block_b(memory_block_a + 16);

                CHECK(object_block_b.get_size() == object_size_b);
                CHECK(object_block_b.check_block(object_block_initialize_value));

                // The object block for the first object should still be there
                CHECK(object_block_a.get_size() == object_size_a);
                CHECK(object_block_a.check_block(object_block_initialize_value));

                // The two objects should be laid out sequentially, and the distance between then could be determined
                // The second object should be "before" the first one by 12 bytes (its own size + 4 bytes for storing the block's size
                CHECK(reinterpret_cast<byte_type*>(pointer_a) - reinterpret_cast<byte_type*>(pointer_b) == 12);
            }
            AND_THEN("This object can be moved into an other (existing) object.")
            {
                memory_manager_type memory_manager_b(memory_manager_type::settings_type(128));

                memory_manager_b = std::move(memory_manager_a);

                void* pointer_b = memory_manager_b.allocate(object_size_b);

                CHECK(pointer_b != nullptr);
                // The start block should still point to the first free block, which should still be at the beginning of the memory block
                CHECK(memory_reader_a.read<size_type>() == 4);
                // Where the start block points, there should be a free block, with only size 8 left
                // and no other free blocks after that
                const free_block_type free_block_b(memory_block_a + 4);

                CHECK(free_block_b.get_size() == 8);
                CHECK(free_block_b.get_next() == 0);
                CHECK(free_block_b.check_block(free_block_initialize_value));

                // A new object block should have been created for the new object with its size
                const object_block_type object_block_b(memory_block_a + 16);

                CHECK(object_block_b.get_size() == object_size_b);
                CHECK(object_block_b.check_block(object_block_initialize_value));

                // The object block for the first object should still be there
                CHECK(object_block_a.get_size() == object_size_a);
                CHECK(object_block_a.check_block(object_block_initialize_value));

                // The two objects should be laid out sequentially, and the distance between then could be determined
                // The second object should be "before" the first one by 12 bytes (its own size + 4 bytes for storing the block's size
                CHECK(reinterpret_cast<byte_type*>(pointer_a) - reinterpret_cast<byte_type*>(pointer_b) == 12);
            }

        }
    }
}

SCENARIO("Deallocating unknown pointer will not cause any trouble")
{
    GIVEN("A memory_manager_type object with no allocations yet")
    {
        memory_manager_type memory_manager(memory_manager_type::settings_type(64));

        AND_GIVEN("A pointer allocated without using the memory_manager_type object")
        {
            int* i = new int(5);

            THEN("Deallocating this pointer will not cause any trouble, but it will not be deleted either")
            {
                memory_manager.deallocate(i);

                CHECK(*i == 5);
                delete i;
            }
        }
    }
}

#ifdef MEMORY_MANAGER_WINDOWS

SCENARIO("memory_manager_type objects are more performant than malloc and free (around twice as fast) -- Use it only on releasy build")
{
    GIVEN("A memory_manager_type object with no allocations yet")
    {
        memory_manager_type memory_manager(memory_manager_type::settings_type(64));
        const int count = 10000;

        THEN("It can handle allocations and deallocations faster than malloc and free")
        {
            const auto malloc_start = std::chrono::high_resolution_clock::now();

            for (int i = 0; i < count; ++i)
            {
                void* pointer = malloc(32);

                free(pointer);
            }

            const auto malloc_stop = std::chrono::high_resolution_clock::now();
            auto malloc_duration = std::chrono::duration_cast<std::chrono::microseconds>(malloc_stop - malloc_start);

            const auto memory_manager_start = std::chrono::high_resolution_clock::now();

            for (int i = 0; i < count; ++i)
            {
                void* pointer = memory_manager.allocate(32);

                memory_manager.deallocate(pointer);
            }
            const auto memory_manager_stop = std::chrono::high_resolution_clock::now();
            auto memory_manager_duration = std::chrono::duration_cast<std::chrono::microseconds>(memory_manager_stop - memory_manager_start);

            CHECK(memory_manager_duration.count() < malloc_duration.count() / 2);
        }
    }
}

#endif
// TODO: Test the effect of minimum_free_block_size
