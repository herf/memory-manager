FROM conanio/gcc7:1.22.2

RUN ["sudo", "apt-get", "update"]
RUN ["sudo", "apt-get", "-y", "install", "git", "ssh", "lcov"]
