#include <iostream>
#include <memory_manager/memory_manager.hh>

using namespace memory_manager;

int main()
{
    memory_manager_type memory_manager(memory_manager_type::settings_type(64));
    void* pointer = memory_manager.allocate(32);

    memory_manager.deallocate(pointer);

    std::cout << "Tests OK" << std::endl;

    return 0;
}
