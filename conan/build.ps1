# Build all possible variations
${archs} = @("x86_64", "x86")
${build_types} = @("Release", "Debug")
${shared_options} = @("True", "False")

foreach(${arch} in ${archs})
{
    foreach(${build_type} in ${build_types})
    {
        foreach(${shared_option} in ${shared_options})
        {
            # Skip shared libs for now
            if(${shared_option} -eq "True")
            {
                continue
            }
            Write-Host -ForegroundColor Green  "----------------------------------------------------------------------------------------"
            Write-Host -ForegroundColor Green  "Creating package with arch=${arch}, build_type=${build_type} and shared=${shared_option}"
            Write-Host -ForegroundColor Green  "----------------------------------------------------------------------------------------"
            conan create -s arch=${arch} -s build_type=${build_type} -o memory-manager:shared=${shared_option} . rudolfheszele/stable
        }
    }
}

git describe --exact-match HEAD 2>&1 | Out-Null
# If we have an annotated tag, we should upload the package
if(${?})
{
    ${tag} = git describe --exact-match HEAD
    # TODO: Add option for remote
    ${remote} = "memory-manager"
    ${package_name} = "memory-manager/${tag}@rudolfheszele/stable"

    Write-Host -ForegroundColor Green  "----------------------------------------------------------------------------------------"
    Write-Host -ForegroundColor Green  "Uploading package to ${remote} as ${package_name}" 
    Write-Host -ForegroundColor Green  "----------------------------------------------------------------------------------------"
    # conan upload -r ${remote} --all ${package_name}
}
else
{
    Write-Host -ForegroundColor Green  "----------------------------------------------------------------------------------------"
    Write-Host -ForegroundColor Green  "No tag found - Skipping package uploading"
    Write-Host -ForegroundColor Green  "----------------------------------------------------------------------------------------"
}
