from conans import ConanFile, CMake, tools
import os


DEVELOPMENT_VERSION = "dev"
CURRENT_VERSION = "v0.5"


def _get_package_version():
    if "MEMORY_MANAGER_DEVELOPMENT" in os.environ.keys():
        return DEVELOPMENT_VERSION
    if "CIRCLE_TAG" in os.environ.keys():
        return os.environ["CIRCLE_TAG"]
    if "CIRCLE_BRANCH" in os.environ.keys():
        return os.environ["CIRCLE_BRANCH"]
    return CURRENT_VERSION


class MemoryManagerConan(ConanFile):
    name = "memory-manager"
    version = _get_package_version()
    license = "<Put the package license here>"
    author = "Rudolf Heszele rudolf.heszele@gmail.com"
    url = "https://bitbucket.org/herf/memory-manager/src/master/"
    description = "Simple memory manager to handle memory allocation and deallocation without malloc/free or new/delete"
    topics = ("memory", "manager")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    generators = ["cmake"]
    exports_sources = ["../*"] if version == DEVELOPMENT_VERSION else None
    _source_folder = "."

    def source(self):
        if MemoryManagerConan.exports_sources is None:
            self.run("git clone --branch {tag} https://herf@bitbucket.org/herf/memory-manager.git {source_folder}".format(tag=MemoryManagerConan.version,
                                                                                                                          source_folder=MemoryManagerConan._source_folder))
            # This small hack might be useful to guarantee proper /MT /MD linkage
            # in MSVC if the packaged project doesn't have variables to set it
            # properly
            tools.replace_in_file("{source_folder}/CMakeLists.txt".format(source_folder=MemoryManagerConan._source_folder),
                                  "project(memory-manager CXX)",
                                  '''project(memory-manager CXX)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()''')
        else:
            # We have everything exported by conan to the current folder, there is nothing to do
            pass

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()
        # Explicit way:
        # self.run('cmake %s/hello %s'
        #          % (self.source_folder, cmake.command_line))
        # self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()
        self.copy("*.hh", dst="include", src="install/include")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.dylib*", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["memory_manager"]

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = "install"
        cmake.definitions["CMAKE_BUILD_TYPE"] = "Release"
        cmake.definitions["MEMORY_MANAGER_STATIC_LIBS"] = "OFF" if self.options["shared"] else "ON"
        cmake.definitions["MEMORY_MANAGER_BUILD_COVERAGE"] = "OFF"
        cmake.definitions["MEMORY_MANAGER_BUILD_TESTS"] = "OFF"
        cmake.configure(source_folder=MemoryManagerConan._source_folder)

        return cmake
