[![CircleCI](https://circleci.com/bb/herf/memory-manager.svg?style=shield)](https://circleci.com/bb/herf/memory-manager)
[![codecov.io](https://codecov.io/bb/herf/memory-manager/coverage.svg?branch=master)](https://codecov.io/bb/herf/memory-manager?branch=master)
[![Bitbucket issues](https://img.shields.io/bitbucket/issues/herf/memory-manager)](https://bitbucket.org/herf/memory-manager/issues)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/c39633aad6ae4e80a46cbff538cb7d06)](https://www.codacy.com/manual/heszele/memory-manager?utm_source=herf@bitbucket.org&amp;utm_medium=referral&amp;utm_content=herf/memory-manager&amp;utm_campaign=Badge_Grade)

A simple memory manager library written in C++11
